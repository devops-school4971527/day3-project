FROM docker:20.10.16

COPY README.md /

ENTRYPOINT cat README.md
CMD echo "nameserver 8.8.8.8" > /etc/resolv.conf
